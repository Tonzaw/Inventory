'use strict';

var app = angular.module('inventoryApp');

// all controllers

app.controller('mainCtrl', function ($scope, $location , Api) {
  $scope.totalValue = 0;
  $scope.inventories = [];
  $scope.userLoggedIn = false;
  
  Api.getUserLoggedIn().then(res => {
    console.log("Checking if user is logged in");
    var user = res.data;
    if (user != null) {
      $scope.userLoggedIn = true;
      $scope.loggedInUsername = user.username;
      console.log($scope.loggedInUsername + " is logged in");
    } else {
      $scope.userLoggedIn = null;
      $location.path('/login');
    }
  });
  
  Api.getAll().then(res => {
    $scope.inventories = res.data;
    console.log("Getting inventories");
    for (var i = 0; i < res.data.length; i++) {
       $scope.totalValue += res.data[i].value;
    }
    $scope.totalBalance = $scope.totalValue;
  }).catch(err => {
    console.log('err:', err);

    $scope.totalValue = $scope.inventories[0].value;
  });

  $scope.addInventory = function() {
    Api.create($scope.newInventory)
    .then(res => {
      $scope.inventories.push($scope.newInventory);
      $scope.totalValue += $scope.newInventory.value || 0;
      $scope.newInventory = {};
      $scope.totalBalance = $scope.totalValue;
    });
  };

  // remove
  $scope.removeInventory = function(inventory) {
    Api.remove(inventory) 
    .then(res => {
     
    })
    .catch(err => {
    console.log('err:', err);
    });

    var index = $scope.inventories.indexOf(inventory);
    $scope.totalValue -= $scope.inventories[index].value || 0;
    $scope.inventories.splice(index, 1);

    // total balance for delete
    $scope.totalBalance = $scope.totalValue;

  };

  //sort when click table header
  $scope.sortBy = function(order) {
    if($scope.sortOrder === order) {
      $scope.sortOrder = `-${order}`;
    } else {
      $scope.sortOrder = order;
    }
  };

});

app.controller('UsersController', function($scope, $location, Api) {
  
  $scope.userLoggedIn = false;
  
  Api.getUserLoggedIn().then(res => {
    console.log("Checking if user is logged in");
    var user = res.data;
    if (user != null) {
      $scope.userLoggedIn = true;
      $scope.loggedInUsername = user.username;
      console.log($scope.loggedInUsername + " is logged in");
    } else {
      $scope.userLoggedIn = null;
      $location.path('/login');
    }
  });
  
  $scope.loginUser = function() {
    Api.login($scope.existingUser)
        .then(res => {
        var user = res.data;
        console.log(user.username + " logged in");
        $scope.loggedInUsername = user.username;
        $location.path('/');
      }).catch(err => {
        if (err.status == 403) {
          alert("Invalid username or password");
        }
      });
  };
  
  $scope.logOut = function() {
    Api.logout().then(res => {
        $scope.userLoggedIn = false;
        $scope.loggedInUsername = "";
        $location.path('/login');
      }).catch(err => {
        alert("Something went wrong");
        console.log('err:', err);
      });
  };

});

app.controller('UserRegisterController', function($scope, $location, Api) {
  
  $scope.addUser = function() {
    if ($scope.newUser.password == $scope.newUser.passwordMatch && $scope.newUser.username != null && $scope.newUser.username != "") {
      Api.register($scope.newUser).then(res => {
        var user = res.data;
        console.log(user + " added");
        $location.path('/login');
      }).catch(err => {
        if (err.status == 403) {
          alert("User already exists");
        }
    });
    } else {
      alert("Passwords don't match or username is invalid");
    }
  };
  
});