'use strict';

var app = angular.module('inventoryApp', ['ngRoute', 'ui.bootstrap']);


app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  .when('/', {
      controller: 'mainCtrl',
      templateUrl: '/views/inventory.ejs'
    })
    .when('/login', {
      controller: 'UsersController',
      templateUrl: '/views/login.ejs'
    })
    .when('/register', {
      controller: 'UserRegisterController',
      templateUrl: '/views/register.ejs'
    })
    .otherwise({
      redirectTo: '/views/login.ejs'
    });
}]);

/*
resolve: {
        userLoggedIn: function($rootScope, Api) {
          return Api.getUserLoggedIn().success(function(user){
            $rootScope.userLoggedIn = user.username ? user : null;
          });
        }
      }
      */