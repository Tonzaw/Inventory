'use strict';

var app = angular.module('inventoryApp');

app.service('Api', function($http) {

  // manage all api calls

  this.getAll = () => {
    return $http.get('/api/inventories');
  };

  this.create = inventory => {
    return $http.post('/api/inventories', inventory);
  };

  this.remove = inventory => {
    return $http.delete(`/api/inventories/${inventory.id}`);
  };

  this.login = user => {
    return $http.post('/api/users/authenticate', user);
  };
  
  this.register = user => {
    return $http.post('/api/users', user);
  };
  
  this.getUserLoggedIn = () => {
    return $http.get('/api/users/logged-in');
  };
  
  this.logout = () => {
    return $http.get('/api/users/logout');
  };

});
