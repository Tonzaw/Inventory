'use strict';

var db = require('../config/db');

db.run(`CREATE TABLE IF NOT EXISTS users (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          username TEXT,
          password TEXT
        )`);

exports.get = function(cb) {
    db.all('SELECT * FROM users', cb);
};

exports.create = function(user, cb) {
    db.run('INSERT INTO users (username, password) VALUES (?, ?)', user.username, user.password,
    (err) => {
      if(err) return cb(err);

      db.get(`SELECT * 
              FROM    users
              WHERE   ID = (SELECT MAX(ID)  FROM users);`, cb);
    });
};

exports.getOneByName = function(name, cb) {
  db.get('SELECT * FROM users WHERE username = ?', name, cb);
};

exports.getOneById = function(id, cb) {
  db.get('SELECT * FROM users WHERE id = ?', id, cb);
};
