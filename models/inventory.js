'use strict';

var db = require('../config/db');

db.run(`CREATE TABLE IF NOT EXISTS inventories (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          item TEXT,
          description TEXT,
          room TEXT,
          value INTEGER
        )`);

exports.get = function(cb) {
  db.all('SELECT * FROM inventories', cb);
};


exports.create = function(inventory, cb) {
  db.run('INSERT INTO inventories (item, description, room, value) VALUES (?, ?, ?, ?)', inventory.item, 
  	inventory.description, inventory.room, inventory.value,
    (err) => {
      if(err) return cb(err);

      db.get(`SELECT * 
              FROM    inventories
              WHERE   ID = (SELECT MAX(ID)  FROM inventories);`, cb)
    });
};



exports.getOneById = function(id, cb) {
  db.get('SELECT * FROM inventories WHERE id = ?', id, cb);
};

exports.removeById = function(id, cb) {
  db.run('DELETE FROM inventories WHERE id = ?', id, cb);
};



