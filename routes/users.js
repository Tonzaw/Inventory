var express = require('express');
var router = express.Router();

var Users = require('../models/userdb');

router.route('/').get((req, res) => {
    Users.get((err, users) => {
      if(err) {
        return res.status(400).send(err);
      }
      res.send(users);
    });
  })

// POST /users
router.post('/', function(req, res, next){
  var user = req.body;
  Users.getOneByName(user.username, (err, userFound) => {
      if(err) {
        return res.status(400).send(err);
      } else {
        if(userFound) {
          return res.status(403).send(err);
        } else {
          Users.create(req.body, (err, newUser) => {
            if(err) {
              return res.status(400).send(err);
            }
            res.send(newUser);
          });
        }
      } 
    });
});

router.post('/authenticate', function(req, res, next){
  var userToCheck = req.body;
  console.log("Authenticating user " + userToCheck.username);
  if(userToCheck == null || userToCheck.username == null || userToCheck.password == null){
    res.sendStatus(403);
  } else {
    Users.getOneByName(userToCheck.username, (err, userFound) => {
      if(err) {
        return res.status(400).send(err);
      } else {
        if(userFound){
          console.log("Found: " + userFound.username + ", with id: " + userFound.id);
          if (userToCheck.password == userFound.password ) {
            req.session.userId = userFound.id;
            res.json(userFound);
          } else {
            console.log("Wrong password");
            res.sendStatus(403);
          }
        }else {
          console.log("User not found");
           res.sendStatus(403);
        }
      }
    });
  }
});


// GET /users/logged-in
router.get('/logged-in', function(req, res, next){
  var loggedInId = req.session.userId ? req.session.userId : null;

  if(loggedInId == null){
    res.json(null);
  }else{
    Users.getOneById(loggedInId, (err, loggedInUser) => {
      if(err) {
        return res.status(400).send(err);
      } else {
        console.log("Logged in user is: " + loggedInUser.username);
        res.json(loggedInUser);
      }
    });
  }
});

// GET /users/logout
router.get('/logout', function(req, res, next) {
  var loggedInId = req.session.userId;
  console.log("logout with sessionid: " + loggedInId);
  req.session.userId = null;
  Users.getOneById(loggedInId, (err, loggedInUser) => {
    if(err) {
      return res.status(400).send(err);
    } else {
      res.json(loggedInUser);
    }
  });
});

module.exports = router;