'use strict';

var express = require('express');
var router = express.Router();

//  GET /
router.get('/', (req, res) => {
  res.render('home');
})

module.exports = router;
