'use strict';

var express = require('express');
var router = express.Router();

router.use('/inventories', require('./inventories'));
router.use('/users', require('./users'));

module.exports = router;
