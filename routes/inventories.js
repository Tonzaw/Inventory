'use strict';

var express = require('express');
var router = express.Router();

var Inventory = require('../models/inventory');

router.route('/')
  .get((req, res) => {
    Inventory.get((err, inventories) => {
      if(err) {
        return res.status(400).send(err);
      }
      res.send(inventories);
    });
  })
  .post((req, res) => {
    Inventory.create(req.body, (err, newInventory) => {
      if(err) {
        return res.status(400).send(err);
      }
      res.send(newInventory);
    });
  });

router.put('/:id/toggle', (req, res) => {
  Inventory.toggle(req.params.id, (err, newValue) => {
    if(err) {
      return res.status(400).send(err);
    }
    res.send({newValue: newValue});
  });
});

router.delete('/:id', (req, res) => {
  Inventory.removeById(req.params.id, err => {
    res.status(err ? 400 : 200).send(err);
  });
});

module.exports = router;
